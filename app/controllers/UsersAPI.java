package controllers;

import play.*;
import play.mvc.*;
import utils.JsonParsers;

import java.util.*;

import com.google.gson.JsonElement;

import models.*;

public class UsersAPI extends Controller
{
  public static void users()
  {
    List<User> users = User.findAll();
    renderJSON(JsonParsers.user2Json(users));
  }

  public static void user(String userId)
  {
    User user = User.findById(userId);  
    if (user == null)
    {
      notFound();
    }
    else
    {
      renderJSON(JsonParsers.user2Json(user));
    }
  }

  public static void createUser(JsonElement body)
  {
    User user = JsonParsers.json2User(body.toString());
    user.save();
    renderJSON(JsonParsers.user2Json(user));
  }

  public static void deleteUser(String id)
  {
	  User user = User.findById(id);
	    if (user == null)
	    {
	      notFound();
	    }
	    else
	    {
	      List<MyTweet> tweets = user.tweets;
	      for(int j = 0; j < tweets.size(); j += 1)
	      {
	        MyTweet tweet = MyTweet.findById(tweets.get(j).uuid);
	        user.tweets.remove(tweet);
	        user.save();
	        tweet.delete();
	      }
	      user.delete();      
	      renderText("success");
	    }
  }

  public static void deleteAllUsers()
  {
	  List <User> users = User.findAll();
	  for(int i = 0; i < users.size(); i += 1)
	  {
		  User user = users.get(i);
		  List <MyTweet> tweets = user.tweets;
		  for(int j = 0; j <tweets.size(); j+= 1)
		  {
			  MyTweet tweet = MyTweet.findById(tweets.get(j).uuid);
			  user.tweets.remove(tweet);
			  user.save();
			  tweet.delete();
		  }
		  user.delete();
	  }
	  renderText("success");
	  }
   
 
  }      