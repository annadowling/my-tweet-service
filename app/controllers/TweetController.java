package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class TweetController extends Controller
{
  public static void index()
  {
    User user = Accounts.getCurrentUser();

    Logger.info("Tweet ctrler : user is " + user.email);
    List<MyTweet> tweets = user.tweets;
    render(user, tweets);
  }

  public static void tweet(String uuid, String message, String dateStamp)
  {
    User user = Accounts.getCurrentUser();
    Logger.info("Tweet id " + uuid + " " + "Message content " + message + " " + "Date" + dateStamp + " " );
    MyTweet tweet = new MyTweet(uuid, message, dateStamp);
    user.tweets.add(tweet);
    user.save();
    index();
  }



  public static void renderReport()
  {
    User user = Accounts.getCurrentUser();
    List<MyTweet> tweets = user.tweets;
    render(tweets);
  }
}
