package controllers;

import java.util.List;
import java.util.Random;
import models.MyTweet;
import models.User;
import play.Logger;
import play.mvc.Controller;
import utils.JsonParsers; 

import com.google.gson.JsonElement;

public class MyTweetServiceAPI extends Controller
{
  /**
   * retrieve all tweets
   */
  public static void tweets(String userId)
  {
	  User user = User.findById(userId);
	    if(user == null)
	    {
	      notFound();
	    }
	    else
	    {
	      List<MyTweet> tweets = user.tweets;
	      renderJSON(JsonParsers.tweet2Json(tweets));
	    }
  }

  public static void tweet(String id)
  {
    MyTweet tweet = MyTweet.findById(id);  
    if (tweet == null)
    {
      notFound();
    }
    else
    {
      renderJSON(JsonParsers.tweet2Json(tweet));
    }
  }

  public static void createTweet(String userId, JsonElement body)
  {
    MyTweet tweet = JsonParsers.json2Tweet(body.toString());
    User user = User.findById(userId);
    //tweet.user = user;
    tweet.save();
    user.tweets.add(tweet);
    user.save();
    renderJSON(JsonParsers.tweet2Json(tweet));
  }

  public static void deleteTweet(String userId, String id)
  {
	User user = User.findById(userId);
    MyTweet tweet = MyTweet.findById(id);
    if (!user.tweets.contains(tweet))
    {
      notFound();
    }
    else
    {
    	user.tweets.remove(tweet);
    	user.save();
        tweet.delete();
        renderText("success");
    }
  }

  public static void deleteAllTweets()
  {
    MyTweet.deleteAll();
    renderText("success");
  }  

}