package models;

import javax.persistence.*;
import play.db.jpa.Model;

@Entity
public class Following extends Model
{
  @ManyToOne()
  public User sourceUser;

  @ManyToOne()
  public User targetUser;

  public Following(User source, User target)
  {
    sourceUser = source;
    targetUser = target;
  }
}