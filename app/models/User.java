package models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

/**
 * User has been escaped: This is necessary because User is a reserved word in PostGreSQL
 * However, if working in local host and wish to use localhost:9000/@db (for example) to view database
 * Then it is necessary to temporarily comment out the line (i.e. @Table(name = "`User`") while testing with local host
 *
 */
@Entity
@Table(name="`User`") //To facilitate deployment to heroku: comment out if required to inspect localhost:9000/@db
public class User extends GenericModel
{
  @Id
  @Column(name="id")
  public String uuid;
  public String firstName;
  public String lastName;
  public String email;
  public String password;

  @OneToMany(cascade = CascadeType.ALL)
  public List<MyTweet> tweets = new ArrayList<MyTweet>();

  public User(String firstName, String lastName, String email, String password)
  {
	this.uuid      = UUID.randomUUID().toString();
    this.firstName= firstName;
    this.lastName = lastName;
    this.email    = email;
    this.password = password;
  }

  public static User findByEmail(String email)
  {
    return find("email", email).first();
  }  

  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  } 
}