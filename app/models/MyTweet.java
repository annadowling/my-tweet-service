package models;

import java.util.UUID;
import java.util.Date;

import play.db.jpa.GenericModel;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.List;

@Entity
public class MyTweet extends GenericModel 
{
	@Id
	@Column(name = "id")
	public String uuid;
	public String message;
	public String dateStamp;
	
	//@ManyToOne
	//public User user;
	
   public MyTweet(String uuid, String message, String dateStamp)
	  {
	   this.uuid = uuid;
	   this.message = message;
	   this.dateStamp = dateStamp;
	    
	  }
}
