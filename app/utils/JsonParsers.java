package utils;

import java.lang.reflect.Type;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import models.MyTweet;
import models.User;

public class JsonParsers
{
  static Gson gson = new Gson();

  public static User json2User(String json)
  {
    return gson.fromJson(json, User.class);   
  }

  public static List<User> json2Users(String json)
  {
    Type collectionType = new TypeToken<List<User>>() {}.getType();
    return gson.fromJson(json, collectionType); 
  }

  public static String user2Json(Object obj)
  {
    return gson.toJson(obj);
  }  

  public static MyTweet json2Tweet(String json)
  {
    return gson.fromJson(json, MyTweet.class);    
  }

  public static String tweet2Json(Object obj)
  {
    return gson.toJson(obj);
  }  

  public static List<MyTweet> json2Tweets(String json)
  {
    Type collectionType = new TypeToken<List<MyTweet>>() {}.getType();
    return gson.fromJson(json, collectionType);    
  }  
}
